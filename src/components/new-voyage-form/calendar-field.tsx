import { CalendarIcon } from "@radix-ui/react-icons";
import { Popover, PopoverContent, PopoverTrigger } from "../ui/popover";
import { Button } from "../ui/button";
import { Calendar } from "../ui/calendar";
import { format } from "date-fns";

interface CalendarFieldProps {
  dates: { from: Date; to: Date };
  //   no exported types it seems
  onDateChange: (range: { from?: Date; to?: Date } | undefined) => void;
}

export const CalendarField = ({ dates, onDateChange }: CalendarFieldProps) => {
  const readableDate =
    dates.from && dates.to ? (
      `${format(dates.from, "PPP")} - ${format(dates.to, "PPP")}`
    ) : (
      <span>Pick a date range</span>
    );

  return (
    <Popover>
      <PopoverTrigger asChild>
        <Button variant="outline">
          {readableDate}
          <CalendarIcon className="ml-auto h-4 w-4 opacity-50" />
        </Button>
      </PopoverTrigger>
      <PopoverContent className="w-auto p-0" align="start">
        <Calendar
          mode="range"
          selected={dates}
          onSelect={onDateChange}
          initialFocus
        />
      </PopoverContent>
    </Popover>
  );
};
