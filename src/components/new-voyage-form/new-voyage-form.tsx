import { z } from "zod";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { Input } from "../ui/input";
import { Label } from "../ui/label";
import {
  Select,
  SelectContent,
  SelectItem,
  SelectTrigger,
  SelectValue,
} from "../ui/select";
import { type VesselsType } from "~/pages/api/vessel/getAll";
import { Button } from "../ui/button";
import { type UnitTypes } from "~/pages/api/unitType/getAll";
import {
  type InvalidateQueryFilters,
  useMutation,
  useQueryClient,
} from "@tanstack/react-query";
import { CalendarField } from "./calendar-field";
import { CheckboxGroup } from "../ui/checkbox-group";
import { useToast } from "../ui/use-toast";

const schema = z
  .object({
    portOfLoading: z.string().min(3),
    portOfDischarge: z.string().min(3),
    vesselId: z.string().min(1),
    scheduledDeparture: z.date(),
    scheduledArrival: z.date(),
    unitTypes: z.array(z.string()).min(5),
  })
  .refine((data) => {
    if (data.scheduledDeparture > data.scheduledArrival) {
      return {
        scheduledDeparture: "Departure date must be before arrival date",
        scheduledArrival: "Arrival date must be after departure date",
      };
    }
    return true;
  });

type FormValues = z.infer<typeof schema>;

interface NewVoyageFormProps {
  vessels: VesselsType;
  unitTypes: UnitTypes;
  onClose: () => void;
}
export const NewVoyageForm = ({
  vessels,
  unitTypes,
  onClose,
}: NewVoyageFormProps) => {
  const { toast } = useToast();
  const queryClient = useQueryClient();

  const mutation = useMutation({
    mutationFn: async (data: FormValues) => {
      const response = await fetch(`/api/voyage/create`, {
        method: "POST",
        body: JSON.stringify({
          departure: data.scheduledDeparture,
          arrival: data.scheduledArrival,
          portOfLoading: data.portOfLoading,
          portOfDischarge: data.portOfDischarge,
          vessel: data.vesselId,
          unitTypes: data.unitTypes,
        }),
        headers: {
          "Content-Type": "application/json",
        },
      });

      if (!response.ok) {
        throw new Error("Failed to create the voyage");
      }
    },
    onSuccess: async () => {
      toast({
        title: "Voyage created",
        description: "The voyage has been created successfully",
      });
      onClose();
      await queryClient.invalidateQueries([
        "voyages",
      ] as InvalidateQueryFilters);
    },
  });

  const {
    register,
    formState: { errors },
    handleSubmit,
    watch,
    setValue,
  } = useForm({
    resolver: zodResolver(schema),
  });

  const departureDate = watch("scheduledDeparture");
  const arrivalDate = watch("scheduledArrival");

  const onSubmit = (data: FormValues) => {
    mutation.mutate(data);
  };

  const handleCheckboxSync = (unitTypes: Record<string, boolean>) => {
    const selectedUnitTypes = Object.entries(unitTypes)
      .filter(([_, value]) => value)
      .map(([key]) => key);

    setValue("unitTypes", selectedUnitTypes);
  };

  return (
    <form
      onSubmit={handleSubmit(onSubmit)}
      className="grid grid-flow-row gap-4 overflow-scroll"
    >
      <div>
        <Label htmlFor="portOfLoading">Port of loading</Label>
        <Input {...register("portOfLoading")} />

        {errors.portOfLoading && (
          <span>{errors.portOfLoading.message?.toString()}</span>
        )}
      </div>

      <div>
        <Label htmlFor="portOfDischarge">Port of discharge</Label>
        <Input {...register("portOfDischarge")} />

        {errors.portOfDischarge && (
          <span>{errors.portOfDischarge.message?.toString()}</span>
        )}
      </div>

      <div>
        <Label htmlFor="vessel">Vessel</Label>
        <Select onValueChange={(v) => setValue("vesselId", v)}>
          <SelectTrigger>
            <SelectValue placeholder="Vessel name" />
          </SelectTrigger>
          <SelectContent>
            {vessels?.map((vessel) => (
              <SelectItem key={vessel.value} value={vessel.value}>
                {vessel.label}
              </SelectItem>
            ))}
          </SelectContent>
        </Select>

        {errors.vesselId && <span>{errors.vesselId.message?.toString()}</span>}
      </div>

      <div className="grid grid-flow-row gap-4">
        <Label htmlFor="scheduledDeparture">Departure and Arrival</Label>
        <CalendarField
          onDateChange={(dateRange) => {
            if (dateRange) {
              setValue("scheduledDeparture", dateRange.from);
              setValue("scheduledArrival", dateRange.to);
            }
          }}
          dates={{ from: departureDate, to: arrivalDate }}
        />

        {errors.scheduledDeparture && (
          <span>{errors.scheduledDeparture.message?.toString()}</span>
        )}
        {errors.scheduledArrival && (
          <span>{errors.scheduledArrival.message?.toString()}</span>
        )}
      </div>

      <div>
        {unitTypes && (
          <CheckboxGroup unitTypes={unitTypes} onChange={handleCheckboxSync} />
        )}
      </div>

      <Button type="submit">Create voyage</Button>
    </form>
  );
};
