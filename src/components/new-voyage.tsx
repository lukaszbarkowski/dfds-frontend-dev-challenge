import { useState } from "react";
import { Button } from "./ui/button";
import {
  Sheet,
  SheetContent,
  SheetDescription,
  SheetHeader,
  SheetTitle,
} from "./ui/sheet";
import { NewVoyageForm } from "./new-voyage-form/new-voyage-form";
import { type VesselsType } from "~/pages/api/vessel/getAll";
import { useQuery } from "@tanstack/react-query";
import { fetchData } from "~/utils";
import { type UnitTypes } from "~/pages/api/unitType/getAll";

export const NewVoyage = () => {
  const [isSheetOpen, setIsSheetOpen] = useState(false);

  return (
    <>
      <div className="flex w-full items-center justify-center border-b border-white border-opacity-50 bg-gray-800 py-4">
        <div className="flex w-full max-w-screen-xl items-center px-4">
          <Button onClick={() => setIsSheetOpen(true)}>
            Create a new voyage
          </Button>
        </div>
      </div>
      <VoyageSheet isOpen={isSheetOpen} onClose={() => setIsSheetOpen(false)} />
    </>
  );
};

interface VoyageSheetProps {
  isOpen: boolean;
  onClose: () => void;
}

const VoyageSheet = ({ isOpen, onClose }: VoyageSheetProps) => {
  const { data: vessels, isLoading: isVesselsLoading } = useQuery<VesselsType>({
    queryKey: ["vessels"],

    queryFn: () => fetchData("vessel/getAll"),
  });

  const { data: unitTypes, isLoading: isUnitTypesLoading } =
    useQuery<UnitTypes>({
      queryKey: ["unitTypes"],

      queryFn: () => fetchData("unitType/getAll"),
    });

  const isLoading = isVesselsLoading || isUnitTypesLoading;

  if (isLoading) {
    return "Loading...";
  }

  return (
    <Sheet open={isOpen} onOpenChange={onClose}>
      <SheetContent>
        <SheetHeader>
          <SheetTitle>New voyage</SheetTitle>
          <SheetDescription>
            {vessels && unitTypes ? (
              <NewVoyageForm
                onClose={onClose}
                vessels={vessels}
                unitTypes={unitTypes}
              />
            ) : (
              <div>
                <p>Couldn't load vessels or unit types data.</p>
                <p>Please try again later.</p>
              </div>
            )}
          </SheetDescription>
        </SheetHeader>
      </SheetContent>
    </Sheet>
  );
};
