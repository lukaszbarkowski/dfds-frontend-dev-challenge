// This one could use generics to make it reusable
// Hacked it a bit as I couldn't find good shadcn component for it

import { useState } from "react";
import { type UnitTypes } from "~/pages/api/unitType/getAll";
import { Checkbox } from "./checkbox";
import { Label } from "./label";

interface CheckboxGroupProps {
  unitTypes: UnitTypes;
  onChange: (unitTypes: Record<string, boolean>) => void;
}

export const CheckboxGroup = ({ unitTypes, onChange }: CheckboxGroupProps) => {
  const [checks, setCheck] = useState<Record<string, boolean>>(
    unitTypes.reduce((acc, current) => {
      return { ...acc, [current.id]: false };
    }, {}),
  );

  const handleCheck = (id: string) => {
    const newChecks = { ...checks, [id]: !checks[id] };

    setCheck(newChecks);
    onChange(newChecks);
  };

  return (
    <div>
      {unitTypes?.map((unitType) => {
        return (
          <div key={unitType.id} className="grid grid-cols-[auto_1fr] gap-2">
            <Checkbox
              onClick={() => handleCheck(unitType.id)}
              id={unitType.id}
            />

            <Label htmlFor={unitType.id}>{unitType.name}</Label>
          </div>
        );
      })}
    </div>
  );
};
